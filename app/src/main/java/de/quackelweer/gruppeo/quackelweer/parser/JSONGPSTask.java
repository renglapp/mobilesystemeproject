package de.quackelweer.gruppeo.quackelweer.parser;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import de.quackelweer.gruppeo.quackelweer.AsyncResponse;
import de.quackelweer.gruppeo.quackelweer.model.Weather;
import de.quackelweer.gruppeo.quackelweer.parser.GPSWebRequest;
import de.quackelweer.gruppeo.quackelweer.parser.WeatherParser;
import de.quackelweer.gruppeo.quackelweer.parser.WeatherWebRequest;


/**
 * Created by Sascha Sevenich on 25.06.18.
 * Edited by Rengo Lapp.
 */
public class JSONGPSTask extends AsyncTask<String, Void, Weather> {


    private final Context context;
    private AsyncResponse callback;
    public static boolean foundWeather = false;


    public JSONGPSTask(Context context, AsyncResponse cb){
        this.context = context;
        this.callback = cb;
    }
    @Override
    protected  Weather doInBackground(String...strings) {
        Weather weather = new Weather();
        String data = ((new GPSWebRequest()).getWeatherData(strings[0]));
        Log.d("TAG","Data vor try catch "+data);
        if(data !=null && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "404"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "city not found"+"\"" +"}") && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "400"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "is not a float"+"\"" +"}")){
            Log.d("TAG","Data in if vor try catch "+data);
            foundWeather = true;

            try {
                Log.d("TAG","Data in try catch "+data);
                weather = WeatherParser.getWeather(data);
                Log.d("TAG",weather.currentCondition.getCondition());
                Log.d("schnell" , "test" + weather);
                return weather;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Log.d("blablabla", "doInBackground: du bist im else: " + foundWeather);
            //foundWeather = false;
            Log.d("blablabla", "du bist im else: " + foundWeather);
            foundWeather = false;
            //cancel(true);
        }
        Log.d("TAG","Data nach try catch "+data);
        Log.d("TAG","Werte: "+weather.temperature.getTemp());
        return null;
    }

    @Override
    protected void onPostExecute(Weather _weather) {
        if(foundWeather){
            callback.asyncResult(_weather);
            Log.d("blabla","onPostexecute:"+ _weather.toString());
        }
        else{
            Log.d("blabla", "onPostExecute: Stadt nicht gefunden");
            Toast.makeText(context, "Fehlerhaft Eingabe. Stadt nicht gefunden" , Toast.LENGTH_LONG).show();
            foundWeather = false;
        }
    }

}
