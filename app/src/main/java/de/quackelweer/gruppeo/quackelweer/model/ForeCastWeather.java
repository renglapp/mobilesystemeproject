package de.quackelweer.gruppeo.quackelweer.model;

/**
 * Created by Marcel Boldt (SMSB) on 21.06.2018.
 *  @Soucre https://www.survivingwithandroid.com/2013/08/android-app-development-forecast-weather-app.html
 */
public class ForeCastWeather {

        public Location location;

        public Condition currentCondition = new Condition();
        public Temperature temperature = new Temperature();

        public Snow snow = new Snow();

        public Rain rain = new Rain();
        public Wind wind = new Wind();

        public class Condition {
                private int weatherID;
                private String condition;
                private String description;

                private String iconID;

                public int getWeatherID() {
                        return weatherID;
                }

                public String getCondition() {
                        return condition;
                }

                public String getDescription() {
                        return description;
                }

                public void setWeatherID(int weatherID) {
                        this.weatherID = weatherID;
                }

                public void setCondition(String condition) {
                        this.condition = condition;
                }

                public void setDescription(String description) {
                        this.description = description;
                }

                public void setIconID(String iconID) {
                        this.iconID = iconID;
                }

                public String getIconID() {
                        return iconID;
                }
        }

        public class Temperature{

                private float minTemp;
                private float maxTemp;
                private float temp;

                public float getMinTemp() {
                        return minTemp;
                }

                public void setMinTemp(float minTemp) {
                        this.minTemp = minTemp;
                }

                public float getMaxTemp() {
                        return maxTemp;
                }

                public void setMaxTemp(float maxTemp) {
                        this.maxTemp = maxTemp;
                }

                public void setTemp(float temp){
                    this.temp = temp;
                }
                public float getTemp(){
                    return temp;
                }
        }




        public class Snow {
                private float ammount;

                public float getAmmount() {
                        return ammount;
                }

                public void setAmmount(float ammount) {
                        this.ammount = ammount;
                }
        }


        public class Rain{
                private float amount;

                public float getAmount(){
                        return amount;
                }
                public void setAmount(float amount){
                        this.amount=amount;
                }
        }
        public class Wind{
            private float speed;
            public float getSpeed(){
                return  speed;
            }
            public void setSpeed(float speed){
                this.speed =speed;
            }
        }

}
