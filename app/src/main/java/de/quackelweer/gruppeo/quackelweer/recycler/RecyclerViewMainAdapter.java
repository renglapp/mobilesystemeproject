package de.quackelweer.gruppeo.quackelweer.recycler;

import  android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import de.quackelweer.gruppeo.quackelweer.DetailActivity;
import de.quackelweer.gruppeo.quackelweer.R;
import de.quackelweer.gruppeo.quackelweer.model.Weather;

/**
 * Created by Marcel Boldt (SMSB) on 16.06.2018.
 * @Source https://developer.android.com/guide/topics/ui/layout/recyclerview
 * @Source https://www.youtube.com/watch?v=94rCjYxvzEE
 */
public class RecyclerViewMainAdapter extends RecyclerView.Adapter<RecyclerViewMainAdapter.ViewHolder> {

    private ArrayList<Weather> weatherAl;
    private Context context;

    public RecyclerViewMainAdapter(ArrayList<Weather> _weather, Context context){
        this.weatherAl = _weather;
        this.context = context;
    }
    @Nullable
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mainrecycler_listitem, parent,false);
        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.city.setText(weatherAl.get(position).location.getCity());
        holder.temp.setText(Math.round(weatherAl.get(position).temperature.getTemp())+" °C");
        holder.condition.setText(weatherAl.get(position).currentCondition.getDescription());
        switch (weatherAl.get(position).currentCondition.getIconID()){
            case "01d":
                    holder.imageView.setImageResource(R.drawable.sonne_leuchtgelb_100x100);
                    break;
            case "02d":
                    holder.imageView.setImageResource(R.drawable.wolkig_lg_100x100);
                    break;
            case "03d":
                    holder.imageView.setImageResource(R.drawable.bewoelkt_sonne_lg_100x100);
                    break;
            case "04d":
                    holder.imageView.setImageResource(R.drawable.bedeckt_100x100);
                    break;
            case "09d":
                    holder.imageView.setImageResource(R.drawable.regen_sonne_lg_100x100);
                    break;
            case "10d":
                    holder.imageView.setImageResource(R.drawable.regen_100x100);
                    break;
            case "11d":
                    holder.imageView.setImageResource(R.drawable.gewitter_100x100);
                    break;
            case "13d":
                    holder.imageView.setImageResource(R.drawable.schnee_100x100);
                    break;
            case "50d":
                    holder.imageView.setImageResource(R.drawable.nebel_sonne_lg_100x100);
                    break;
            case "01n":
                holder.imageView.setImageResource(R.drawable.mond_leuchtgelb_100x100);
                break;
            case "02n":
                holder.imageView.setImageResource(R.drawable.wolkig_mond_lg_100x100);
                break;
            case "03n":
                holder.imageView.setImageResource(R.drawable.bewoelkt_mond_lg_100x100);
                break;
            case "04n":
                holder.imageView.setImageResource(R.drawable.bedeckt_100x100);
                break;
            case "09n":
                holder.imageView.setImageResource(R.drawable.regen_mond_lg_100x100);
                break;
            case "10n":
                holder.imageView.setImageResource(R.drawable.regen_100x100);
                break;
            case "11n":
                holder.imageView.setImageResource(R.drawable.gewitter_100x100);
                break;
            case "13n":
                holder.imageView.setImageResource(R.drawable.schnee_100x100);
                break;
            case "50n":
                holder.imageView.setImageResource(R.drawable.nebel_mond_leuchtgelb_100x1);
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailActivity.class).putExtra("city",weatherAl.get(position).location.getCity().toString());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return weatherAl.size();

    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView city;
        TextView temp;
        TextView condition;
        ImageView imageView;

        public ViewHolder(View itemView){
            super(itemView);
            city = itemView.findViewById(R.id.textViewCity);
            temp = itemView.findViewById(R.id.textViewTemp);
            condition = itemView.findViewById(R.id.textViewCondition);
            imageView = itemView.findViewById(R.id.weather_icon);
        }
    }

    public void removeItem(int position){

        weatherAl.remove(position);
        notifyItemRemoved(position);





    }

}



