package de.quackelweer.gruppeo.quackelweer;

import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;
import de.quackelweer.gruppeo.quackelweer.model.Weather;

/**
 * Created by Marcel Boldt (SMSB) on 15.06.2018.
 */
public interface AsyncResponse {
    void asyncResult(Weather _weather);
}
