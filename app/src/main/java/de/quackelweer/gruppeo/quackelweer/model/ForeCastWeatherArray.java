package de.quackelweer.gruppeo.quackelweer.model;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Marcel Boldt (SMSB) on 22.06.2018.
 */
public class ForeCastWeatherArray {
    private ArrayList<ForeCastWeather> foreCastWeatherAl = new ArrayList<>();
    public void addForeCast(ForeCastWeather foreCast){
        foreCastWeatherAl.add(foreCast);
    }
    public ForeCastWeather getForeCast(int num){
        return foreCastWeatherAl.get(num);
    }
    public int getLength(){
        int lenght = foreCastWeatherAl.size();
        return lenght;
    }
}
