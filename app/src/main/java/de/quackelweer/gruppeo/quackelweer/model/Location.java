package de.quackelweer.gruppeo.quackelweer.model;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Rengo Lapp on 28.05.18.
 * Edited by Marcel Boldt
 * Source: https://www.survivingwithandroid.com/2013/05/android-openweathermap-app-weather-app.html
 */

public class Location {

    private String sunset;
    private String sunrise;
    private String country;
    private String city;
    private String time;
    private SimpleDateFormat sdfSun = new SimpleDateFormat("HH:mm");

    private SimpleDateFormat sdfDay = new SimpleDateFormat("dd.MM HH:mm");



    public String getSunset() {
        return sunset;
    }

    public void setSunset(String _sunset) {
        long sunlong = Long.parseLong(_sunset);
        sunlong = sunlong*1000;
        Date date = new Date(sunlong);
        _sunset = sdfSun.format(date);
        this.sunset = _sunset;

    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        long sunlong = Long.parseLong(sunrise);
        sunlong = sunlong*1000;
        Date date = new Date(sunlong);
        sunrise = sdfSun.format(date);
        this.sunrise = sunrise;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public void setTime(String time){
        long timelong = Long.parseLong(time);
        timelong = timelong*1000;
        Date date = new Date(timelong);
        time = sdfDay.format(date);
        this.time = time;

    }
    public String getTime(){
        return time;
    }
}
