package de.quackelweer.gruppeo.quackelweer.model;


/**
 * Created by Rengo Lapp on 28.05.18.
 * Source: https://www.survivingwithandroid.com/2013/05/android-openweathermap-app-weather-app.html
 */

public class Weather {
    public Location location;

    public Condition currentCondition = new Condition();
    public Temperature temperature = new Temperature();
    public Wind wind = new Wind();
    public Snow snow = new Snow();
    public Clouds clouds = new Clouds();


    public class Condition {
        private int weatherID;
        private String condition;
        private String description;

        private float pressure;
        private float humidity;
        private String iconID;

        public int getWeatherID() {
            return weatherID;
        }

        public String getCondition() {
            return condition;
        }

        public String getDescription() {
            return description;
        }

        public float getPressure() {
            return pressure;
        }

        public float getHumidity() {
            return humidity;
        }

        public void setWeatherID(int weatherID) {
            this.weatherID = weatherID;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setPressure(float pressure) {
            this.pressure = pressure;
        }

        public void setHumidity(float humidity) {
            this.humidity = humidity;
        }

        public void setIconID(String iconID) {
            this.iconID = iconID;
        }

        public String getIconID() {
            return iconID;
        }
    }

    public class Temperature{
        private float temp;
        private float minTemp;
        private float maxTemp;

        public float getTemp() {
            return temp;
        }

        public void setTemp(float temp) {
            this.temp = temp;
        }

        public float getMinTemp() {
            return minTemp;
        }

        public void setMinTemp(float minTemp) {
            this.minTemp = minTemp;
        }

        public float getMaxTemp() {
            return maxTemp;
        }

        public void setMaxTemp(float maxTemp) {
            this.maxTemp = maxTemp;
        }
    }

    public class Wind{
        private float speed;
        private float deg;

        public float getSpeed() {
            return speed;
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }

        public float getDeg() {
            return deg;
        }

        public void setDeg(float deg) {
            this.deg = deg;
        }
    }


    public class Snow {
        private String time;
        private float ammount;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public float getAmmount() {
            return ammount;
        }

        public void setAmmount(float ammount) {
            this.ammount = ammount;
        }
    }

    public class Clouds{
        private int perc;

        public int getPerc() {
            return perc;
        }

        public void setPerc(int perc) {
            this.perc = perc;
        }
    }

}
