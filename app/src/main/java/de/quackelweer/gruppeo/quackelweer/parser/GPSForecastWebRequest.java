package de.quackelweer.gruppeo.quackelweer.parser;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GPSForecastWebRequest {
    private static String Base_URL = "http://api.openweathermap.org/data/2.5/forecast?";
    private static String apiKey1="a37263163cfa89d5f52878b193c2096c";
    private static String apiKey2="51cf7951ddc786c0e48403365340ef21";
    private static String metric ="&units=metric";
    private static String language ="&lang=de";

    public String getWeatherData (String location) {
        HttpURLConnection connection = null;
        InputStream inputStream = null;

        String url = Base_URL + location + language + metric;

        if (location != null) {
            url = url + "&appid=" + apiKey1;
            Log.d("TAG", "Weblink: " + url);
            try {

                //Verbindungsaufbau
                connection = (HttpURLConnection) (new URL(url)).openConnection();
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.connect();

                //Auslesen der Antwort
                StringBuffer buffer = new StringBuffer();
                inputStream = connection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    buffer.append(line + "\r\n");
                inputStream.close();
                connection.disconnect();
                Log.d("ForeCast", "Buffer: " + buffer.toString());
                return buffer.toString();
            } catch (Throwable t) {
                t.printStackTrace();
            } finally {
                try {
                    // inputStream.close();
                    inputStream = null;
                } catch (Throwable t) {
                    t.printStackTrace();
                }
                try {
                    connection.disconnect();
                } catch (Throwable t) {
                    t.printStackTrace();
                }

            }


            return null;
        }

        return null;
    }
}