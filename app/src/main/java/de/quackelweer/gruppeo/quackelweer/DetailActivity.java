package de.quackelweer.gruppeo.quackelweer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeatherArray;
import de.quackelweer.gruppeo.quackelweer.model.Weather;
import de.quackelweer.gruppeo.quackelweer.parser.JSONForeCastWeatherTask;
import de.quackelweer.gruppeo.quackelweer.parser.JSONGPSForecastTask;
import de.quackelweer.gruppeo.quackelweer.parser.JSONGPSTask;
import de.quackelweer.gruppeo.quackelweer.parser.JSONWeatherTask;
import de.quackelweer.gruppeo.quackelweer.recycler.RecyclerViewDetailAdapter;

/**
 * Created by Marcel Boldt (SMSB) on 18.06.2018.
 */

public class DetailActivity extends AppCompatActivity implements AsyncResponse,AsyncResponseForeCast {


    private ArrayList<ForeCastWeather> foreCastWeatherAl = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);                                          //BackButton oben Links
        Intent intentStr = getIntent();                                                                 //Intent erstellen
        String city = intentStr.getStringExtra("city");                                            //Werte vom Intent aus der SearchView (MainActivity)

        if(!city.contains("lat") && !city.contains("lon")) {
            launchTask(city);                                                                               //starten der JSONWeatherTask
            launchTaskForeCast(city);
        }else{
            launchGpsTask(city);
            launchGpsForecastTask(city);

        }

    }

    private void launchGpsForecastTask(String _city) {
        JSONGPSForecastTask task = new JSONGPSForecastTask(getApplicationContext(), this);
        task.execute(_city);

    }

    private void launchGpsTask(String _city) {
        JSONGPSTask task = new JSONGPSTask(getApplicationContext(), this);
        task.execute(_city);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu_detail, menu);

        return true;

    }*/


    /**
     * Rückgabe der Werte aus JSONWeatherTask über Interface AsyncResponse
     * Views der DetailActivity gesetzt
     *
     * @param _weather
     * @Author Marcel boldt
     */
    @Override
    public void asyncResult(final Weather _weather) {


        TextView cityTextView = findViewById(R.id.textViewCity);
        TextView tempTextView = findViewById(R.id.textViewTemp);
        TextView dateTextView = findViewById(R.id.textViewDate);
        ImageView imageView = findViewById(R.id.imageView);
        TextView humidityTextView = findViewById(R.id.textViewHumidity);
        TextView pressureTextView = findViewById(R.id.textViewPressure);
        TextView speedtextView = findViewById(R.id.textViewSpeed);
        TextView sunriseTextView = findViewById(R.id.textViewSunrise);
        TextView sunsetTextView = findViewById(R.id.textViewSunset);
        cityTextView.setText(_weather.location.getCity());

        tempTextView.setText(Math.round(_weather.temperature.getTemp()) + "°C");
        String time = _weather.location.getTime();
        time = time.substring(0, 5);
        dateTextView.setText(time);
        humidityTextView.setText("Luftfeuchtigkeit: " + Math.round(_weather.currentCondition.getHumidity()) + " %");
        pressureTextView.setText("Luftdruck: " + Math.round(_weather.currentCondition.getPressure()) + " hPa");
        speedtextView.setText("Wingeschwindigkeit: " + Float.toString(_weather.wind.getSpeed()) + " m/s");
        sunriseTextView.setText("Sonnenaufgang: " + _weather.location.getSunrise());
        sunsetTextView.setText("Sonnenuntergang: " + _weather.location.getSunset());
        ActionMenuItemView item = findViewById(R.id.favorie_city);
        /*item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(_weather.location.getCity());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("city", jsonArray.toString());
                editor.commit();
                Toast.makeText(getApplicationContext(), "Wurde hinzugefügt", Toast.LENGTH_SHORT).show();
            }
        });*/


        switch (_weather.currentCondition.getIconID()) {
            case "01d":
                imageView.setImageResource(R.drawable.sonne_leuchtgelb_100x100);
                break;
            case "02d":
                imageView.setImageResource(R.drawable.wolkig_lg_100x100);
                break;
            case "03d":
                imageView.setImageResource(R.drawable.bewoelkt_sonne_lg_100x100);
                break;
            case "04d":
                imageView.setImageResource(R.drawable.bedeckt_100x100);
                break;
            case "09d":
                imageView.setImageResource(R.drawable.regen_sonne_lg_100x100);
                break;
            case "10d":
                imageView.setImageResource(R.drawable.regen_100x100);
                break;
            case "11d":
                imageView.setImageResource(R.drawable.gewitter_100x100);
                break;
            case "13d":
                imageView.setImageResource(R.drawable.schnee_100x100);
                break;
            case "50d":
                imageView.setImageResource(R.drawable.nebel_sonne_lg_100x100);
                break;
            case "01n":
                imageView.setImageResource(R.drawable.mond_leuchtgelb_100x100);
                break;
            case "02n":
                imageView.setImageResource(R.drawable.wolkig_mond_lg_100x100);
                break;
            case "03n":
                imageView.setImageResource(R.drawable.bewoelkt_mond_lg_100x100);
                break;
            case "04n":
                imageView.setImageResource(R.drawable.bedeckt_100x100);
                break;
            case "09n":
                imageView.setImageResource(R.drawable.regen_mond_lg_100x100);
                break;
            case "10n":
                imageView.setImageResource(R.drawable.regen_100x100);
                break;
            case "11n":
                imageView.setImageResource(R.drawable.gewitter_100x100);
                break;
            case "13n":
                imageView.setImageResource(R.drawable.schnee_100x100);
                break;
            case "50n":
                imageView.setImageResource(R.drawable.nebel_mond_leuchtgelb_100x1);
                break;
        }
    }

    /**
     * Starten der JSONWeatherTask
     * Eingabe der Stadt
     * Stadt wird übergeben
     *
     * @param _city Eingabe der Stadt
     * @Author Marcel Boldt
     */
    private void launchTask(String _city) {
        JSONWeatherTask task = new JSONWeatherTask(getApplicationContext(), this);
        task.execute(_city);

    }


    /**
     * Starten der JSONForeCastTask
     * Eingabe der Stadt
     * Stadt wird übergeben
     *
     * @param _city
     * @Author Marcel Boldt
     */
    private void launchTaskForeCast(String _city) {
        JSONForeCastWeatherTask task = new JSONForeCastWeatherTask(getApplicationContext(), this);
        task.execute(_city);
    }

    /**
     * Rückgabe der Werte aus JSONForeCastTask über Interface AsyncResponseForeCast
     *
     * @param foreCastWeatherArray
     * @Author Marcel Boldt
     */
    @Override
    public void asyncResultForeCast(ForeCastWeatherArray foreCastWeatherArray) {

        for (int i = 0; i < foreCastWeatherArray.getLength(); i++) {
            foreCastWeatherAl.add(foreCastWeatherArray.getForeCast(i));
        }
        initRecyclerView();
    }

    /**
     * BackButton in ActionBar
     *
     * @Problem Teilweise doppelter Aufruf DetailActivity beim Drücken,
     * Tests zeigten nur Probleme im Emulator
     * Smartphone keine Probleme
     * @Author Sascha Sevenich
     */
    @Override
    public boolean onSupportNavigateUp() {
        this.finish();

        return true;
    }

    /**
     * @Author Marcel Boldt
     * @Source https://developer.android.com/guide/topics/ui/layout/recyclerview
     * @Source https://www.youtube.com/watch?v=94rCjYxvzEE
     */
    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.detail_recycler);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewDetailAdapter adapter = new RecyclerViewDetailAdapter(foreCastWeatherAl, this);
        recyclerView.setAdapter(adapter);
    }
}


