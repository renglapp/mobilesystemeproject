package de.quackelweer.gruppeo.quackelweer.parser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import de.quackelweer.gruppeo.quackelweer.AsyncResponse;
import de.quackelweer.gruppeo.quackelweer.DetailActivity;
import de.quackelweer.gruppeo.quackelweer.MainActivity;
import de.quackelweer.gruppeo.quackelweer.R;
import de.quackelweer.gruppeo.quackelweer.model.Weather;

import static android.content.ContentValues.TAG;

/**
 * Created by Marcel Boldt (SMSB) on 11.06.2018.
 * Edited by Rengo Lapp.
 * @Source: https://www.survivingwithandroid.com/2013/05/android-openweathermap-app-weather-app.html
 */


public class JSONWeatherTask extends AsyncTask<String,Void,Weather> {
    private final Context context;
    private AsyncResponse callback;
    public static boolean foundWeather = false;


    public JSONWeatherTask(Context context,AsyncResponse cb){
        this.context = context;
        this.callback = cb;
    }
    @Override
    protected  Weather doInBackground(String... objects) {
        Weather weather = new Weather();
        String data = ((new WeatherWebRequest()).getWeatherData(objects[0]));
        Log.d("TAG","Data vor try catch "+data);
        if(data !=null && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "404"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "city not found"+"\"" +"}")){
            Log.d("TAG","Data in if vor try catch "+data);
            foundWeather = true;

            try {
                Log.d("TAG","Data in try catch "+data);
                weather = WeatherParser.getWeather(data);
                Log.d("TAG",weather.currentCondition.getCondition());
                Log.d("schnell" , "test" + weather);
                return weather;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Log.d("blablabla", "doInBackground: du bist im else: " + foundWeather);
            //foundWeather = false;
            Log.d("blablabla", "du bist im else: " + foundWeather);
            foundWeather = false;
            //cancel(true);
        }
        Log.d("TAG","Data nach try catch "+data);
        Log.d("TAG","Werte: "+weather.temperature.getTemp());
        return null;
    }

    @Override
    protected void onPostExecute(Weather _weather) {
        if(foundWeather){
        callback.asyncResult(_weather);
        Log.d("blabla","onPostexecute:"+ _weather.toString());
        }
        else{
            Log.d("blabla", "onPostExecute: Stadt nicht gefunden");
            Toast.makeText(context, "Fehlerhaft Eingabe. Stadt nicht gefunden" , Toast.LENGTH_LONG).show();
            foundWeather = false;
        }
    }

}
