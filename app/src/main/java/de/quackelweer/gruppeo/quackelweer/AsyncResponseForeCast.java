package de.quackelweer.gruppeo.quackelweer;

import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeatherArray;

/**
 * Created by Marcel Boldt (SMSB) on 21.06.2018.
 */
public interface AsyncResponseForeCast {
    void asyncResultForeCast(ForeCastWeatherArray foreCastWeatherArray);
}
