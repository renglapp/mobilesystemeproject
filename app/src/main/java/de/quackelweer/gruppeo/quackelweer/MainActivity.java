package de.quackelweer.gruppeo.quackelweer;

import android.Manifest;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SearchView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import de.quackelweer.gruppeo.quackelweer.GPS.GPSLocator;
import de.quackelweer.gruppeo.quackelweer.model.Weather;
import de.quackelweer.gruppeo.quackelweer.parser.JSONGPSTask;
import de.quackelweer.gruppeo.quackelweer.parser.JSONWeatherTask;
import de.quackelweer.gruppeo.quackelweer.recycler.RecyclerViewMainAdapter;


/**
 * Created by Rengo Lapp on 28.05.18.
 * Edited by Marcel Boldt.
 */

public class MainActivity extends AppCompatActivity implements AsyncResponse {
    SearchView searchView;
    OnClickListener onMenuClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            createInfoDialog("more").show();
        }

    };
    private ArrayList<Weather> weatherAl = new ArrayList<>();
    private ArrayList<String> cityAl = new ArrayList<>();
    private RecyclerViewMainAdapter adapter;
    private ActionMenuItemView gpsView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapter = new RecyclerViewMainAdapter(weatherAl,this);




        cityAl.add("Berlin");
        cityAl.add("Hamburg");
        cityAl.add("Kiel");
        cityAl.add("Düsseldorf");
        cityAl.add("Hannover");
        cityAl.add("Mainz");
        cityAl.add("Wiesbaden");
        cityAl.add("Saarbrücken");
        cityAl.add("Potsdam");
        cityAl.add("Schwerin");
        cityAl.add("Erfurt");
        cityAl.add("Dresden");
        cityAl.add("Magdeburg");
        cityAl.add("Bremen");
        cityAl.add("Stuttgart");
        cityAl.add("München");
        //cityAl.add("Stralsund");*/


        for(int b = 0;b<cityAl.size();b++){
            launchTask(cityAl.get(b));
        }

    }

    /**
     * Erstellen der Menüsuchleiste
     * Eingabe in die Suchleiste
     * Übergabe der Suche an die DetailActivity
     * @Author: Rengo Lapp
     * @Author: Marcel Boldt
     * @Source: https://javapapers.com/android/android-searchview-action-bar-tutorial/
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu,menu);

        final SearchManager searchManager =(SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo((getComponentName())));
        searchView.setQueryHint("Bitte Stadt eingeben");                                                                        //Hint setzen
        searchView.setIconified(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {                                                //QueryTextListener
            @Override
            public boolean onQueryTextSubmit(String query) {                                                                    //Klicken der Suche in der tastatur
                if(isOnline() ) {
                    String city = searchView.getQuery().toString();                                                             //aktuellen Wert aus der Query(Suchleiste)

                    Intent intent = new Intent(getApplicationContext(), DetailActivity.class).putExtra("city", city);     //Intent mit Inhalt Query an DetailActivity
                    getApplicationContext().startActivity(intent);                                                              //Activity mit Intent starten

                }else{                                                                                                          // Wenn kein Internet, dann Activity Restart
                    Toast.makeText(getBaseContext(), "No internet connection found" , Toast.LENGTH_LONG).show();
                    searchView = findViewById(R.id.search);
                    searchView.setQuery("",false);
                    searchView.setIconified(true);
                }return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    /**
     * Führt die JSONWeatherTask aus
     * @Author Marcel Boldt
     * @param location Eingabe der gesuchten Stadt
     */
    private void launchTask(String location){
        if(isOnline()) {
            JSONWeatherTask task = new JSONWeatherTask(getApplicationContext(), this);
            task.execute(location);
        }else{
            //Log.d("hallo", "hier bin ich");
            JSONWeatherTask task = new JSONWeatherTask(getApplicationContext(), this);
            Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Weather Rückgabe aus der JSONWeatherTask
     * Weather wird zur ArrayList weatherAl hinzugefügt
     * RecyclerView wird initialisiert
     * @Author Marcel Boldt
     * @param _weather
     */
    @Override
    public void asyncResult(Weather _weather) {
        weatherAl.add(_weather);
        initRecyclerView();
        Log.d("TAG", "Main: "+ weatherAl.isEmpty());
    }

    /**
     * Initialisierung der RecyclerView
     * ItemTouchHelper fürs swipen der RecyclerViewItems
     * @Author Marcel Boldt
     * @Source https://developer.android.com/guide/topics/ui/layout/recyclerview
     * @Source https://www.youtube.com/watch?v=94rCjYxvzEE
     * @Source https://www.androidhive.info/2017/09/android-recyclerview-swipe-delete-undo-using-itemtouchhelper/
     */
    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        final RecyclerView recyclerView = findViewById(R.id.main_recycler);
        RecyclerViewMainAdapter adapter = new RecyclerViewMainAdapter(weatherAl,this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                RecyclerViewMainAdapter recyclerViewMainAdapter =(RecyclerViewMainAdapter) recyclerView.getAdapter();
                Log.d("ViewHolder", "Position: "+viewHolder.getAdapterPosition());
                recyclerViewMainAdapter.removeItem(viewHolder.getAdapterPosition());
                Log.d("ViewHolder","Größe vor: "+cityAl.get(viewHolder.getAdapterPosition()+1));
                cityAl.remove(viewHolder.getAdapterPosition()+1);
                SharedPreferences sharedPreferences = getSharedPreferences("favorite_city",MODE_PRIVATE);
                sharedPreferences.edit().remove("city").commit();
                //Log.d("ViewHolder","Größe nach: "+cityAl.get(viewHolder.getAdapterPosition()+1));

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        searchView = findViewById(R.id.search);
        searchView.setQuery("",false);
        searchView.setIconified(true);                                                              //nach Activitywechsel, Searchview wird geschlossen, muss nach setQuery() kommen
    }

    /**
     * Ueberpruefung ob Internetverbindung besteht
     * @author Rengo Lapp
     * @return true
     */
    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("favorite_city",MODE_PRIVATE);
        try {
            JSONArray jsonArray = new JSONArray(sharedPreferences.getString("city","[]"));
                for(int i =0;i<jsonArray.length();i++){
                    cityAl.add(jsonArray.getInt(i)+"");
                    launchTask(jsonArray.getInt(i)+"");
                    Log.d("city","nwnwdn"+jsonArray.getInt(i)+"");
                }
            } catch (JSONException e1) {
            e1.printStackTrace();
        }

    }
    public AlertDialog createInfoDialog(String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Informationen zur App");
        builder.setMessage("Die Daten für diese App werden von OpenWeatherMap.com zur Verfügung gestellt. \nWeitere Informationen unter: \nhttps://openweathermap.org/");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        return builder.create();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        item.getItemId();
        switch (item.getItemId()) {
            case R.id.more:
                createInfoDialog("more").show();
                break;
            case R.id.gps:

                gpsView = findViewById(R.id.gps);
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);

                GPSLocator gpsLocator = new GPSLocator(getApplicationContext());
                Location location = gpsLocator.GetLocation();
                if(location != null)
                {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    Log.d("position" , "Position bei" + latitude);
                    Log.d("position" , "Position bei" + longitude);
                    launchGpsTask(latitude, longitude);
                    String loca = "lat="+Double.toString(latitude) + "&lon=" + Double.toString(longitude);
                    Intent intent = new Intent(getApplicationContext() , DetailActivity.class).putExtra("city" , loca);
                    getApplicationContext().startActivity(intent);
                }
                return true;
        }

        return true;
    }
    private void launchGpsTask(double latitude, double longitude){
        if(isOnline()) {
            String location = "lat="+Double.toString(latitude) + "&lon=" + Double.toString(longitude);
            JSONGPSTask task = new JSONGPSTask(getApplicationContext(), this);
            task.execute(location);
        }else{
            //Log.d("hallo", "hier bin ich");
            JSONGPSTask task = new JSONGPSTask(getApplicationContext(), this);
            Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

//     class ViewClick implements View.OnClickListener {
//         @Override
//         public void onClick(View v) {
//           gpsClicked();
//         }
//         private void gpsClicked() {
//             gpsView = findViewById(R.id.gps);
//             ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
//             gpsView.setOnClickListener(new ViewClick());
//
//             GPSLocator gpsLocator = new GPSLocator(getApplicationContext());
//             Location location = gpsLocator.GetLocation();
//             if(location != null)
//             {
//                 double latitude = location.getLatitude();
//                 double longitude = location.getLongitude();
//                 Log.d("position" , "Position bei" + latitude);
//             }
//
//         }
//
//     }


     }
