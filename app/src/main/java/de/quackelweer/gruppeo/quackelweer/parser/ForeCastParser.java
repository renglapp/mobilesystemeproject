package de.quackelweer.gruppeo.quackelweer.parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeatherArray;
import de.quackelweer.gruppeo.quackelweer.model.Location;


/**
 * Created by Marcel Boldt (SMSB) on 21.06.2018.
 */
public class ForeCastParser {
    public static ForeCastWeatherArray getWeather(String data) throws JSONException {
        ForeCastWeatherArray foreCastWeatherArray = new ForeCastWeatherArray();
        Log.d("ForeCastData","Weatherparser Data: "+data);

        //JSON Objekt aus Datei erstellen
        JSONObject jsonObject = new JSONObject(data);
        Log.d("ForeCast","Data: "+data);
        JSONArray jsonArray = jsonObject.getJSONArray("list");
        for(int index =0 ;index<jsonArray.length();index++){
            JSONObject jForeCast = jsonArray.getJSONObject(index);
            ForeCastWeather foreCastWeather = new ForeCastWeather();
            Location location = new Location();
            Log.d("Time","toString(): "+jForeCast.toString());
            foreCastWeather.location = location;
            if(jForeCast.toString().contains("{"+"\""+"dt"+"\""+":")){
                int start = jForeCast.toString().indexOf("{"+"\""+"dt"+"\""+":");
                String time = jForeCast.toString().substring(start+6,start+16);
                foreCastWeather.location.setTime(time);

            }else{
                foreCastWeather.location.setTime("0");

            }

            //Wetter setzen
            JSONArray jsonWeatherArray = jForeCast.getJSONArray("weather");
            JSONObject jsonObjectWeather = jsonWeatherArray.getJSONObject(0);
            foreCastWeather.currentCondition.setWeatherID(getInt("id",jsonObjectWeather));
            foreCastWeather.currentCondition.setCondition(getString("main",jsonObjectWeather));
            foreCastWeather.currentCondition.setDescription(getString("description",jsonObjectWeather));
            foreCastWeather.currentCondition.setIconID(getString("icon",jsonObjectWeather));

            //Temperaturen
            JSONObject mainObject = getObject("main",jForeCast);
            foreCastWeather.temperature.setTemp(getFloat("temp",mainObject));
            foreCastWeather.temperature.setMaxTemp(getFloat("temp_max",mainObject));
            foreCastWeather.temperature.setMinTemp(getFloat("temp_min",mainObject));


            //Regen
            if(jForeCast.toString().contains("rain")){
                JSONObject rainObject = getObject("rain",jForeCast);
                if(rainObject.toString().contains("3h")){
                    foreCastWeather.rain.setAmount(getFloat("3h",rainObject));

                }else {
                    foreCastWeather.rain.setAmount(0);
                    Log.d("Rain","Regenmenge: "+foreCastWeather.rain.getAmount());
                }
            }

            //Schnee
            if(jForeCast.toString().contains("snow")){
                JSONObject snowObject = getObject("snow",jForeCast);
                if(snowObject.toString().contains("3h")){
                    foreCastWeather.snow.setAmmount(getFloat("3h",snowObject));
                }else{
                    foreCastWeather.snow.setAmmount(0);
                }
            }
            JSONObject windObject = getObject("wind",jForeCast);
            foreCastWeather.wind.setSpeed(getFloat("speed",windObject));



            foreCastWeatherArray.addForeCast(foreCastWeather);

        }

        return foreCastWeatherArray;
    }

    private static JSONObject getObject(String tagName, JSONObject jsonObject)  throws JSONException {
        JSONObject subObj = jsonObject.getJSONObject(tagName);
        return subObj;
    }
    private static String getString(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(tagName);
    }
    private static float  getFloat(String tagName, JSONObject jsonObject) throws JSONException {
        return (float) jsonObject.getDouble(tagName);
    }
    private static int  getInt(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getInt(tagName);
    }


}

