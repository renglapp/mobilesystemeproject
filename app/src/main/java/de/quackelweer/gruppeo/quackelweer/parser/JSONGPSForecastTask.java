package de.quackelweer.gruppeo.quackelweer.parser;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import de.quackelweer.gruppeo.quackelweer.AsyncResponseForeCast;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeatherArray;
import de.quackelweer.gruppeo.quackelweer.parser.ForeCastParser;
import de.quackelweer.gruppeo.quackelweer.parser.ForeCastWeatherWebRequest;
import de.quackelweer.gruppeo.quackelweer.parser.GPSForecastWebRequest;

/**
 * Created by Sascha Sevenich on 25.06.18.
 * Edited by Rengo Lapp.
 */
public class JSONGPSForecastTask extends AsyncTask<String, Void, ForeCastWeatherArray>{
    private final Context context;
    private AsyncResponseForeCast callback;
    public static boolean foundWeather = false;

    public JSONGPSForecastTask(Context context,AsyncResponseForeCast cb){
        this.context = context;
        this.callback = cb;
    }

    protected void onPostExecute(ForeCastWeatherArray foreCastWeatherArray) {
        if(foundWeather) {
            callback.asyncResultForeCast(foreCastWeatherArray);
        }else{

            // DetailActivity activity = new DetailActivity();
            // activity.getApplicationContext();

            //DetailActivity.getContext().getApplicationContext();
            //DetailActivity.mContext.getApplicationContext();
            foundWeather = false;
        }
        //foundWeather = false;
    }


    @Override
    protected ForeCastWeatherArray doInBackground(String... strings) {
        ForeCastWeatherArray foreCastWeatherArray;
        String data = ((new GPSForecastWebRequest()).getWeatherData(strings[0]));

        if(data !=null && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "404"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "city not found"+"\"" +"}") && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "400"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "is not a float"+"\"" +"}")){
            foundWeather = true;
            try{
                Log.d("ForeCast","Data:"+ data);
                foreCastWeatherArray = ForeCastParser.getWeather(data);
                return foreCastWeatherArray;
            }catch(JSONException e){
                e.printStackTrace();
            }
        }else{
            //cancel(true);
            foundWeather = false;

        }
        return null;
    }

}
