package de.quackelweer.gruppeo.quackelweer.recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.quackelweer.gruppeo.quackelweer.R;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;

/**
 * Created by Marcel Boldt (SMSB) on 18.06.2018.
 * @Source https://developer.android.com/guide/topics/ui/layout/recyclerview
 * @Source https://www.youtube.com/watch?v=94rCjYxvzEE
 *
 * @Problem Bei einer Suchanfrage im Emulator wird die Recyclerview mit einem grauen Schatten überlegt
 *          Auf einem Smartphone keine Probleme
 */
public class RecyclerViewDetailAdapter extends RecyclerView.Adapter<RecyclerViewDetailAdapter.ViewHolder> {
    private ArrayList<ForeCastWeather> foreCastWeatherAl;
    private Context context;
    public RecyclerViewDetailAdapter(ArrayList<ForeCastWeather> foreCastWeatherAl, Context context){
        this.foreCastWeatherAl = foreCastWeatherAl;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detailrecycler_listitem,parent,false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.temp.setText("Temperatur: "+Math.round(foreCastWeatherAl.get(position).temperature.getTemp())+" °C");
        holder.rain.setText("Regen: "+foreCastWeatherAl.get(position).rain.getAmount()+" mm");
        holder.time.setText(""+foreCastWeatherAl.get(position).location.getTime());
        holder.wind.setText("Wind: "+foreCastWeatherAl.get(position).wind.getSpeed()+" m/s");
        switch (foreCastWeatherAl.get(position).currentCondition.getIconID()){
            case "01d":
                holder.image.setImageResource(R.drawable.sonne_leuchtgelb_100x100);
                break;
            case "02d":
                holder.image.setImageResource(R.drawable.wolkig_lg_100x100);
                break;
            case "03d":
                holder.image.setImageResource(R.drawable.bewoelkt_sonne_lg_100x100);
                break;
            case "04d":
                holder.image.setImageResource(R.drawable.bedeckt_100x100);
                break;
            case "09d":
                holder.image.setImageResource(R.drawable.regen_sonne_lg_100x100);
                break;
            case "10d":
                holder.image.setImageResource(R.drawable.regen_100x100);
                break;
            case "11d":
                holder.image.setImageResource(R.drawable.gewitter_100x100);
                break;
            case "13d":
                holder.image.setImageResource(R.drawable.schnee_100x100);
                break;
            case "50d":
                holder.image.setImageResource(R.drawable.nebel_sonne_lg_100x100);
                break;
            case "01n":
                holder.image.setImageResource(R.drawable.mond_leuchtgelb_100x100);
                break;
            case "02n":
                holder.image.setImageResource(R.drawable.wolkig_mond_lg_100x100);
                break;
            case "03n":
                holder.image.setImageResource(R.drawable.bewoelkt_mond_lg_100x100);
                break;
            case "04n":
                holder.image.setImageResource(R.drawable.bedeckt_100x100);
                break;
            case "09n":
                holder.image.setImageResource(R.drawable.regen_mond_lg_100x100);
                break;
            case "10n":
                holder.image.setImageResource(R.drawable.regen_100x100);
                break;
            case "11n":
                holder.image.setImageResource(R.drawable.gewitter_100x100);
                break;
            case "13n":
                holder.image.setImageResource(R.drawable.schnee_100x100);
                break;
            case "50n":
                holder.image.setImageResource(R.drawable.nebel_mond_leuchtgelb_100x1);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return foreCastWeatherAl.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView time;
        TextView temp;
        TextView rain;
        TextView wind;
        ImageView image;
        public ViewHolder(View itemView){
            super(itemView);
            time = itemView.findViewById(R.id.textViewTime);
            temp = itemView.findViewById(R.id.textViewTemp);
            wind = itemView.findViewById(R.id.textViewWind);
            rain = itemView.findViewById(R.id.textViewRain);
            image = itemView.findViewById(R.id.imageView);
        }
    }
}
