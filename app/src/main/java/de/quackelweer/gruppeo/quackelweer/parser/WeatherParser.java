package de.quackelweer.gruppeo.quackelweer.parser;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import de.quackelweer.gruppeo.quackelweer.model.Weather;
import de.quackelweer.gruppeo.quackelweer.model.Location;


/**
 * Created by Rengo Lapp on 17.05.18.
 * Edited by Marcel Boldt.
 * Source: https://www.survivingwithandroid.com/2013/05/android-openweathermap-app-weather-app.html
 */

public class WeatherParser {
    public static Weather getWeather(String data) throws JSONException {
        Weather weather = new Weather();
        Log.d("Time","Weatherparser Data: "+data);

        //JSON Objekt aus Datei erstellen
        JSONObject jsonObject = new JSONObject(data);

        /**Extrahieren der Daten**/
        Location location = new Location();

        JSONObject sysObj = getObject("sys", jsonObject);
        location.setCountry(getString("country", sysObj));
        location.setSunrise(getString("sunrise", sysObj));
        location.setSunset(getString("sunset", sysObj));
        location.setCity(getString("name", jsonObject));
        weather.location = location;
        //Log.d("TAG","Weatherwerte "+weather.location.getCity());

        /**get Weather info in Array **/
        JSONArray jsonArray = jsonObject.getJSONArray("weather");

        /**Wert an erster Stelle nutzen **/
        JSONObject JSONWeather = jsonArray.getJSONObject(0);
        weather.currentCondition.setWeatherID(getInt("id" , JSONWeather));
        weather.currentCondition.setDescription(getString("description" , JSONWeather));
        weather.currentCondition.setCondition(getString("main" , JSONWeather));
        weather.currentCondition.setIconID(getString("icon",JSONWeather));

        JSONObject mainObj = getObject("main" , jsonObject);
        weather.temperature.setTemp(getFloat("temp" , mainObj));
        weather.currentCondition.setPressure(getFloat("pressure" , mainObj));
        weather.currentCondition.setHumidity(getFloat("humidity" , mainObj));
        weather.temperature.setMinTemp(getFloat("temp_min" , mainObj));
        weather.temperature.setMaxTemp(getFloat("temp_max" , mainObj));

        //Clouds
        JSONObject cloudObject = getObject("clouds" , jsonObject);
        weather.clouds.setPerc(getInt("all" , cloudObject));


        //Snow
        if(data.contains(","+"\""+"snow"+"\""+":")){
            JSONObject snowObject = getObject("snow" , jsonObject);
            weather.snow.setTime(getString("snowTime" , snowObject));
            weather.snow.setAmmount(getFloat("snowAmmount" , snowObject));

        }else{
            weather.snow.setTime("0");
            weather.snow.setAmmount(0);
        }
        //Zeit
        if(data.contains(","+"\""+"dt"+"\""+":")){
            int start = data.indexOf(","+"\""+"dt"+"\""+":");
            String time = data.substring(start+6,start+16);
            weather.location.setTime(time);
            Log.d("Time","Die Zeit ist: "+ time);
        }else{
            weather.location.setTime("0");
            Log.d("Time","Die Zeit wurde nicht gefunden");
        }


        //Wind
        JSONObject windObject = getObject("wind" , jsonObject);
        weather.wind.setSpeed(getFloat("speed" , windObject));
        if(data.contains(","+"\""+"deg"+"\""+":")){
            weather.wind.setDeg(getFloat("deg" , windObject));
        }else{
            weather.wind.setDeg(0);
        }

        return weather;
    }

    private static JSONObject getObject(String tagName, JSONObject jsonObject)  throws JSONException {
        JSONObject subObj = jsonObject.getJSONObject(tagName);
        return subObj;
    }
    private static String getString(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(tagName);
    }
    private static float  getFloat(String tagName, JSONObject jsonObject) throws JSONException {
        return (float) jsonObject.getDouble(tagName);
    }
    private static int  getInt(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getInt(tagName);
    }



}
