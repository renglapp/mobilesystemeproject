package de.quackelweer.gruppeo.quackelweer.parser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;

import de.quackelweer.gruppeo.quackelweer.AsyncResponse;
import de.quackelweer.gruppeo.quackelweer.AsyncResponseForeCast;
import de.quackelweer.gruppeo.quackelweer.DetailActivity;
import de.quackelweer.gruppeo.quackelweer.MainActivity;
import de.quackelweer.gruppeo.quackelweer.R;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeather;
import de.quackelweer.gruppeo.quackelweer.model.ForeCastWeatherArray;
import de.quackelweer.gruppeo.quackelweer.model.Weather;

/**
 * Created by Marcel Boldt (SMSB) on 20.06.2018.
 * @Source: https://www.survivingwithandroid.com/2013/05/android-openweathermap-app-weather-app.html
 */
public class JSONForeCastWeatherTask extends AsyncTask<String, Void, ForeCastWeatherArray> {
    private final Context context;
    private AsyncResponseForeCast callback;
    public static boolean foundWeather = false;

    public JSONForeCastWeatherTask(Context context,AsyncResponseForeCast cb){
        this.context = context;
        this.callback = cb;
    }

    protected void onPostExecute(ForeCastWeatherArray foreCastWeatherArray) {
        if(foundWeather) {
            callback.asyncResultForeCast(foreCastWeatherArray);
        }else{

           // DetailActivity activity = new DetailActivity();
           // activity.getApplicationContext();

            //DetailActivity.getContext().getApplicationContext();
            //DetailActivity.mContext.getApplicationContext();
            foundWeather = false;
        }
        //foundWeather = false;
    }


    @Override
    protected ForeCastWeatherArray doInBackground(String... strings) {
        ForeCastWeatherArray foreCastWeatherArray;
        String data = ((new ForeCastWeatherWebRequest()).getWeatherData(strings[0]));

        if(data !=null && !data.contains("{"+"\""+"cod"+"\"" + ":" +"\""+ "404"+"\"" + ","+"\"" +"message"+"\"" + ":"+"\"" + "city not found"+"\"" +"}")){
            foundWeather = true;
            try{
                Log.d("ForeCast","Data:"+ data);
                foreCastWeatherArray = ForeCastParser.getWeather(data);
                return foreCastWeatherArray;
            }catch(JSONException e){
                e.printStackTrace();
            }
        }else{
            //cancel(true);
            foundWeather = false;

        }
        return null;
    }

}
